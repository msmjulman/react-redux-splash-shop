import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { cartReducer } from './reducers/cartReducer';
import { productReducer } from './reducers/productReducers';

const rootReducer = combineReducers({
	products: productReducer,
	cart: cartReducer,
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
	rootReducer,
	composeEnhancer(applyMiddleware(logger, thunk))
);

export default store;
