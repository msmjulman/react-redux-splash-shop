import * as actions from './type';

export const addToCartAction = (product, cart) => {
	const cartItems = cart.slice();
	let isAlreadyInCart = false;
	cartItems.forEach((item) => {
		if (item._id === product._id) {
			isAlreadyInCart = true;
		}
	});
	if (!isAlreadyInCart) {
		cartItems.push({ ...product, count: 1 });
	}

	localStorage.setItem('cartItems', JSON.stringify(cartItems));

	return {
		type: actions.ADD_TO_CART,
		payload: { cartItems: cartItems },
	};
};

export const removeFromCartAction = (cart, id) => {
	const cartItems = cart.filter((item) => item._id !== id);

	localStorage.setItem('cartItems', JSON.stringify(cartItems));

	return {
		type: actions.REMOVE_FROM_CART,
		payload: { cartItems: cartItems },
	};
};

export const clearCartAction = () => {
	localStorage.removeItem('cartItems');

	return {
		type: actions.ADD_ANOTHER_PRODUCT,
		payload: { cartItems: [] },
	};
};

export const addAnotherProductAction = (product, cart) => {
	const cartItems = cart.slice();
	cartItems.forEach((item) => {
		if (item._id === product._id) {
			item.count++;
		}
	});

	localStorage.setItem('cartItems', JSON.stringify(cartItems));

	return {
		type: actions.ADD_ANOTHER_PRODUCT,
		payload: { cartItems: cartItems },
	};
};

export const removeAnProductAction = (product, cart) => {
	const cartItems = cart.slice();
	cartItems.forEach((item) => {
		if (item._id === product._id && item.count > 1) {
			item.count--;
		}
	});

	localStorage.setItem('cartItems', JSON.stringify(cartItems));

	return {
		type: actions.REMOVE_PRODUCT,
		payload: { cartItems: cartItems },
	};
};
