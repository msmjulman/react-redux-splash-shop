import * as actions from './type';
import data from '../../data/data.json';
import { sortItemRules } from '../../utils/utils';

export const loadDataAction = () => ({
	type: actions.LOAD_PRODUCTS,
	payload: data.products,
});

export const filterByCategoryAction = (products, category, color) => {
	const filteredProductsByColor =
		color && color !== 'all'
			? products.filter((product) => product._color === color)
			: products;

	return {
		type: actions.FILTER_BY_CATEGORY,
		payload: {
			// color: color,
			category: category,
			filteredItems:
				category !== 'all'
					? filteredProductsByColor.filter(
							(product) => product._category === category
					  )
					: filteredProductsByColor,
		},
	};
};

export const filterByColorAction = (products, category, color) => {
	const filteredProductsByCat =
		category && category !== 'all'
			? products.filter((product) => product._category === category)
			: products;

	return {
		type: actions.FILTER_BY_COLOR,
		payload: {
			color: color,
			filteredItems:
				color !== 'all'
					? filteredProductsByCat.filter((product) => product._color === color)
					: filteredProductsByCat,
		},
	};
};

export const clearFiltersAction = (products, category, color) => {
	const filteredProductsByCat =
		category && category !== 'all'
			? products.filter((product) => product._category === category)
			: products;

	return {
		type: actions.CLEAR_FILTER,
		payload: {
			items: color
				? filteredProductsByCat.filter((product) => product._color !== color)
				: filteredProductsByCat,
		},
	};
};

export const sortProductsAction = (products, sort) => {
	const sortedProducts = products.slice();
	sortedProducts.sort((a, b) => sortItemRules(sort, a, b));

	return {
		type: actions.SORT_PRODUCTS,
		payload: { sort: sort, filteredItems: sortedProducts },
	};
};

export const toggleSidebarFilterAction = () => {
	return {
		type: actions.TOGGLE_SIDEBAR_FILTER,
	};
};
