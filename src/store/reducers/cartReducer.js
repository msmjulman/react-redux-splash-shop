import * as actions from './../actions/type';

const intialState = {
	cartItems: JSON.parse(localStorage.getItem('cartItems')) || [],
};

export const cartReducer = (state = intialState, action) => {
	switch (action.type) {
		case actions.ADD_TO_CART:
			return { ...state, cartItems: action.payload.cartItems };

		case actions.REMOVE_FROM_CART:
			return { ...state, cartItems: action.payload.cartItems };

		case actions.CLEAR_CART:
			return { ...state, cartItems: action.payload.cartItems };

		case actions.ADD_ANOTHER_PRODUCT:
			return { ...state, cartItems: action.payload.cartItems };

		case actions.REMOVE_PRODUCT:
			return { ...state, cartItems: action.payload.cartItems };

		default:
			return state;
	}
};
