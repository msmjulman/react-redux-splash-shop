import * as actions from '../actions/type';

const initialState = {
	items: [],
	filteredItems: [],
	color: 'all',
	toggle: false,
};

export const productReducer = (state = initialState, action) => {
	switch (action.type) {
		case actions.LOAD_PRODUCTS:
			return { ...state, items: action.payload, filteredItems: action.payload };

		case actions.FILTER_BY_COLOR:
			return {
				...state,
				color: action.payload.color,
				filteredItems: action.payload.filteredItems,
			};

		case actions.FILTER_BY_CATEGORY:
			return {
				...state,
				category: action.payload.category,
				filteredItems: action.payload.filteredItems,
			};

		case actions.CLEAR_FILTER:
			return {
				...state,
				color: action.payload.color,
				size: action.payload.size,
				filteredItems: action.payload.items,
			};

		case actions.SORT_PRODUCTS:
			return {
				...state,
				sort: action.payload.sort,
				filteredItems: action.payload.filteredItems,
			};

		case actions.TOGGLE_SIDEBAR_FILTER:
			return { ...state, toggle: !state.toggle };

		default:
			return state;
	}
};
