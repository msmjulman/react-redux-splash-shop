import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Wrapper from './components/LayoutWrapper';
import ProductDetails from './components/ProductDetails';
import ProductList from './components/ProductList';
import Cart from './components/Cart';

function App() {
	return (
		<div className="App">
			<Router>
				<Wrapper>
					<Switch>
						<Route exact path="/" component={ProductList} />
						<Route
							exact
							path="/product/:id/details"
							component={ProductDetails}
						/>
						<Route exact path="/cart" component={Cart} />
					</Switch>
				</Wrapper>
			</Router>
		</div>
	);
}

export default App;
