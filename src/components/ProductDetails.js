import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { addToCartAction } from '../store/actions/cartActions';
import { loadDataAction } from '../store/actions/productActions';
import { inCartChecker, moneyFormat } from '../utils/utils';
import './ProductDetails.css';

class ProductDetails extends Component {
	componentDidMount() {
		this.props.loadProducts();
	}

	render() {
		const {
			products,
			cart,
			addToCart,
			match: {
				params: { id },
			},
		} = this.props;
		const filteredProducts = products.filter((prod) => prod._id === id);

		return (
			<div className="content content-details p-lg-5">
				{filteredProducts.map((product) => (
					<div className="product-container" key={product._id}>
						<div className="product-title-container text-center mb-4 ">
							<h1>{product._title}</h1>
						</div>
						<div className="row">
							<div className="col-lg-6">
								<div className="img-container ">
									<img
										src={product._image}
										alt={`product_${product._id}`}
										className="img-fluid w-100"
									/>
								</div>
							</div>
							<div className="col-lg-6">
								<div className="product-details ">
									<h4 className="product-title my-4">{product._title}</h4>
									<p className="product-description mb-4 text-justify">
										{product._description}
									</p>
									<p className="product-price mb-4 ">
										<span className="price-label font-weight-bold mr-2">
											Price :
										</span>
										{moneyFormat(product._price)}
									</p>
									<p className="button-container d-flex flex-row justify-content-end mb-0">
										<Link to="/" className="btn rounded-0 d-inline-block mr-3">
											<i className="fa fa-chevron-left"></i> Back
										</Link>
										{inCartChecker(cart, product._id) ? (
											<span className="btn rounded-0 d-inline-block">
												In Cart
											</span>
										) : (
											<button
												onClick={() => addToCart(product, cart)}
												className="btn rounded-0 d-inline-block">
												Add To Cart
											</button>
										)}
									</p>
								</div>
							</div>
						</div>
					</div>
				))}
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	products: state.products.items,
	cart: state.cart.cartItems,
});

const mapDispatchToProps = (dispatch) => ({
	loadProducts: () => dispatch(loadDataAction()),
	addToCart: (product, cart) => dispatch(addToCartAction(product, cart)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
