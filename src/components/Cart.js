import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
	addAnotherProductAction,
	removeAnProductAction,
	removeFromCartAction,
	clearCartAction,
} from '../store/actions/cartActions';
import { moneyFormat, pluralize } from '../utils/utils';
import './Cart.css';

function Cart({
	cart,
	removeFromCart,
	clearCart,
	addAnotherProduct,
	removeAnProduct,
}) {
	return (
		<div className="content content-cart p-5">
			<div className="cart-container">
				<Link className="btn rounded-0 mb-4" to="/">
					<i className="fa fa-chevron-left mr-2"></i>Back
				</Link>
				<div className="cart-header mb-4 d-flex justify-content-between align-items-center">
					{cart.length !== 0 ? (
						<span className="cart-infos d-inline-block">{`You have ${
							cart.length
						} product${pluralize(cart.length)}.`}</span>
					) : (
						<div className="text-center w-100">
							<span className="cart-infos ">
								Your cart is empty, add a product.{' '}
							</span>
						</div>
					)}
					{cart.length !== 0 && (
						<button
							className="btn cart-clear rounded-0"
							onClick={() => clearCart()}>
							Clear Cart
						</button>
					)}
				</div>
				<div className="cart-body">
					{cart.map((item) => (
						<div className="item-container mb-2 border-bottom" key={item._id}>
							<div className="row">
								<div className="col-md-6">
									<div className="cart-item-info d-flex align-items-center mb-2">
										<img
											src={item._image}
											alt={`item_${item._id}`}
											className="img-fluid w-25"
										/>
										<span className="item-title ml-5">{item._title}</span>
									</div>
								</div>
								<div className="col-md-6 d-flex align-items-center">
									<div className="cart-item-action d-flex justify-content-around align-items-center w-100 mb-2">
										<p className="mb-0">
											<button
												onClick={() => removeAnProduct(item, cart)}
												className="btn rounded-0">
												<i className="fa fa-minus"></i>
											</button>
											<span className="item-count mx-3">{`x ${item.count}`}</span>
											<button
												onClick={() => addAnotherProduct(item, cart)}
												className="btn rounded-0">
												<i className="fa fa-plus"></i>
											</button>
										</p>

										<span className="item-count">
											{moneyFormat(
												cart
													.filter((x) => x._id === item._id)
													.reduce((a, c) => a + c._price * c.count, 0)
											)}
										</span>
										<button
											onClick={() => removeFromCart(cart, item._id)}
											className="btn rounded-0">
											<i className="fa fa-trash"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					))}
				</div>
				<div className="cart-footer text-right">
					{cart.length !== 0 && (
						<p className="cart-total mb-0 font-weight-bold">
							{`Total : ${moneyFormat(
								cart.reduce((a, c) => a + c._price * c.count, 0)
							)}`}
						</p>
					)}
				</div>
			</div>
		</div>
	);
}

const mapStateToProps = (state) => ({ cart: state.cart.cartItems });

const mapDispatchToProps = (dispatch) => ({
	removeFromCart: (item, id) => dispatch(removeFromCartAction(item, id)),
	clearCart: () => dispatch(clearCartAction()),
	addAnotherProduct: (product, cart) =>
		dispatch(addAnotherProductAction(product, cart)),
	removeAnProduct: (product, cart) =>
		dispatch(removeAnProductAction(product, cart)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
