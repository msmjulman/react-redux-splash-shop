import React from 'react';
import { connect } from 'react-redux';
import {
	filterByCategoryAction,
	filterByColorAction,
	sortProductsAction,
	loadDataAction,
	clearFiltersAction,
} from '../store/actions/productActions';
import './SidebarFilter.css';

function SidebarFilter({
	color,
	category,
	filterByCategory,
	filterByColor,
	clearFilters,
	sortProducts,
	filteredProducts,
	products,
}) {
	return (
		<div className="side-content my-2 px-4 pt-4 pb-1">
			<div className="filter-box mb-4">
				<p className="filter-title">Categories :</p>
				<div className="form-group mb-0">
					<select
						className="form-control"
						name="size"
						id="size"
						onChange={(e) => filterByCategory(products, e.target.value, color)}>
						<option value="all">All</option>
						<option value="uniforms">Uniforms</option>
						<option value="shoes">Shoes</option>
						<option value="protections">Protections</option>
					</select>
				</div>
			</div>
			<div className="filter-box mb-4">
				<p className="filter-title mb-2">Filter by Color :</p>
				<div className="form-check ">
					<label className="form-check-label d-lg-block" htmlFor="all">
						<input
							type="radio"
							className="form-check-input "
							name="color"
							id="all"
							defaultValue="all"
							defaultChecked={true || color === 'all'}
							onChange={(e) => clearFilters(products, category, e.target.value)}
						/>
						All
					</label>
					<label className="form-check-label d-lg-block" htmlFor="colored">
						<input
							type="radio"
							className="form-check-input"
							name="color"
							id="colored"
							defaultValue="colored"
							onChange={(e) =>
								filterByColor(products, category, e.target.value)
							}
						/>
						Colored
					</label>
					<label className="form-check-label d-lg-block" htmlFor="black">
						<input
							type="radio"
							className="form-check-input"
							name="color"
							id="black"
							defaultValue="black"
							onChange={(e) =>
								filterByColor(products, category, e.target.value)
							}
						/>
						Black
					</label>
					<label className="form-check-label d-lg-block" htmlFor="white">
						<input
							type="radio"
							className="form-check-input"
							name="color"
							id="white"
							defaultValue="white"
							onChange={(e) =>
								filterByColor(products, category, e.target.value)
							}
						/>
						White
					</label>
				</div>
			</div>
			<div className="filter-box mb-4">
				<p className="filter-title">Sort Products :</p>
				<div className="form-group mb-0">
					<select
						className="form-control"
						name="size"
						id="size"
						onChange={(e) => sortProducts(filteredProducts, e.target.value)}>
						<option value="">Select</option>
						<option value="lastest">Lastest</option>
						<option value="lowest">Lowest Price</option>
						<option value="highest">Highest Price</option>
					</select>
				</div>
			</div>
		</div>
	);
}

const mapStateToProps = (state) => ({
	products: state.products.items,
	filteredProducts: state.products.filteredItems,
	color: state.products.color,
	category: state.products.category,
});

const mapDispatchToProps = (dispatch) => ({
	loadProducts: () => dispatch(loadDataAction()),
	filterByCategory: (products, category, color) =>
		dispatch(filterByCategoryAction(products, category, color)),
	filterByColor: (products, category, color) =>
		dispatch(filterByColorAction(products, category, color)),
	clearFilters: (products, category, color, size) =>
		dispatch(clearFiltersAction(products, category, color, size)),
	sortProducts: (products, sort) =>
		dispatch(sortProductsAction(products, sort)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarFilter);
