import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './ProductList.css';
import { inCartChecker, moneyFormat, pluralize } from '../utils/utils';
import SidebarFilter from './SidebarFilter';
import {
	loadDataAction,
	toggleSidebarFilterAction,
} from '../store/actions/productActions';
import { connect } from 'react-redux';
import { addToCartAction } from '../store/actions/cartActions';

class ProductList extends Component {
	state = {
		toggle: false,
	};
	componentDidMount() {
		this.props.loadProducts();
	}

	toggleSideBar() {
		this.setState({ toggle: !this.state.toggle });
	}

	render() {
		const {
			products,
			cart,
			addToCart,
			toggle,
			toggleSidebarFilter,
		} = this.props;

		return (
			<div>
				<div className="row">
					<div
						className={`sidebar-container col-lg-3 order-lg-last d-lg-block ${
							!toggle ? 'd-none' : ''
						}`}>
						<SidebarFilter />
					</div>
					<div className="col-lg-9 order-lg-first">
						<div className="content px-4 my-2">
							{products.length === 0 ? (
								<div className="text-center py-5">
									<h2 className="font-weight-light">
										No product for your selection.
									</h2>
								</div>
							) : (
								<>
									<div className="product-count py-3 d-flex justify-content-between align-items-center">
										<span className="font-weight-bolder">{`${
											products.length
										} product${pluralize(products.length)}`}</span>
										<button
											onClick={() => toggleSidebarFilter()}
											className="btn d-lg-none font-weight-bold">
											<i className="fa fa-sliders-h"></i>
										</button>
									</div>
									<div className="row">
										{products.map((product) => (
											<div
												className="col-lg-3 col-md-4 col-sm-6"
												key={product._id}>
												<div className="card mb-4 rounded-0">
													<div className="image-container text-center p-2">
														<Link to={`/product/${product._id}/details`}>
															<img
																className="card-img-top w-75 img-fluid"
																src={product._image}
																alt={`product_${product._id}`}
															/>
														</Link>
													</div>
													<div className="card-body pt-2 px-2 pb-2">
														<p className="card-title">{product._title}</p>
														<div className="card-product-footer">
															<span className="d-inline-block w-50 text-center">
																{moneyFormat(product._price)}
															</span>
															{inCartChecker(cart, product._id) ? (
																<span className="btn incart-status rounded-0 d-inline-block w-50">
																	In Cart{inCartChecker(cart, product._id)}
																</span>
															) : (
																<button
																	onClick={() => addToCart(product, cart)}
																	className="btn  rounded-0 d-inline-block w-50">
																	<i className="fa fa-cart-plus"></i>
																</button>
															)}
														</div>
													</div>
												</div>
											</div>
										))}
									</div>
								</>
							)}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	products: state.products.filteredItems,
	toggle: state.products.toggle,
	cart: state.cart.cartItems,
});

const mapDispatchToProps = (dispatch) => ({
	loadProducts: () => dispatch(loadDataAction()),
	addToCart: (product, cart) => dispatch(addToCartAction(product, cart)),
	toggleSidebarFilter: () => dispatch(toggleSidebarFilterAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
