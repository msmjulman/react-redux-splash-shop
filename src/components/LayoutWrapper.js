import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './LayoutWrapper.css';

function LayoutWrapper(props) {
	return (
		<>
			<nav className="navbar navbar-expand-lg navbar-light fixed-top ">
				<div className="container">
					<Link className="navbar-brand" to="/">
						<img src="/images/splash-logo.png" alt="logo" />
					</Link>
					<Link to="/cart" className="btn pb-0 pr-0 mt-2 cart-button">
						<i className="fa fa-shopping-cart"></i>
						<span className="cart-item-count">{props.cart.length}</span>
					</Link>
				</div>
			</nav>

			<section id="main-container" className="py-3">
				<div className="container">{props.children}</div>
			</section>

			<footer>
				<div className="container">
					<div id="footer-container">
						<div id="copyright-container">
							&copy; Copyright 2020 - Powered By MSM
						</div>
						<div id="logo-container">
							<Link to="/">
								<img src="/images/splash-logo.png" alt="logo" />
							</Link>
						</div>
					</div>
				</div>
			</footer>
		</>
	);
}

const mapStateToProps = (state) => ({
	cart: state.cart.cartItems,
});

export default connect(mapStateToProps, null)(LayoutWrapper);
