export function pluralize(length) {
	let plural = '';
	if (length > 1) {
		plural = 's';
	}
	return plural;
}

export function moneyFormat(num) {
	return `$${Number(num.toFixed(2))}`;
}

export function sortItemRules(word, params1, params2) {
	switch (word) {
		case 'lowest':
			if (params1._price > params2._price) {
				return 1;
			} else {
				return -1;
			}
		case 'highest':
			if (params1._price < params2._price) {
				return 1;
			} else {
				return -1;
			}
		case 'lastest':
			if (params1._id < params2._id) {
				return 1;
			} else {
				return -1;
			}
		default:
			if (params1._id > params2._id) {
				return 1;
			} else {
				return -1;
			}
	}
}

export function inCartChecker(cart, id) {
	let answer = false;
	cart.forEach((item) => {
		if (item._id === id) {
			answer = true;
		}
	});
	return answer;
}
